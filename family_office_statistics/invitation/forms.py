from __future__ import absolute_import, unicode_literals

from django import forms



CATEGORY_CHOICES = (
	("Single Family Office Executive", "Single Family Office Executive"),
	("Ultra High Net Worth", "Ultra High Net Worth"),
	("Multi Family Office", "Multi Family Office"),
	("Institutional Investor", "Institutional Investor"),
	("RIA", "RIA")
)

class ContactForm(forms.Form):
	first_name = forms.CharField(required=True, label="First name",widget=forms.TextInput(attrs={'class' : 'form-field-class'}))
	last_name = forms.CharField(required=True, label="Last name",widget=forms.TextInput(attrs={'class' : 'form-field-class'}))
	email = forms.EmailField(required=True, label="Email",widget=forms.EmailInput(attrs={'class' : 'form-field-class'}))
	category = forms.ChoiceField(required=True, choices=CATEGORY_CHOICES, label="Type of Investor",widget=forms.Select(attrs={'class':'form-field-class'}))