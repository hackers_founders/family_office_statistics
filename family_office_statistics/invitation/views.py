from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail
from django.core.urlresolvers import reverse_lazy
from django.template.loader import render_to_string
from django.views.generic import FormView

from .forms import ContactForm


class InvitationView(SuccessMessageMixin, FormView):
    template_name = "request_an_invite.html"
    form_class = ContactForm
    success_url = reverse_lazy('home')
    success_message = "Your message was sent."

    def form_valid(self, form):
        context_dict = {
            'first_name': form.cleaned_data['first_name'],
            'last_name': form.cleaned_data['last_name'],
            'email': form.cleaned_data['email'],
            'category': form.cleaned_data['category']
        }

        msg = render_to_string('email.txt', context_dict)

        send_mail(subject='New FOS Invitaion Request', message=msg, from_email=settings.EMAIL_HOST_USER,
                  recipient_list=[settings.EMAIL_HOST_USER], fail_silently=False)
    
        return super(InvitationView, self).form_valid(form)
