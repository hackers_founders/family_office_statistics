# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.defaults import bad_request, permission_denied, page_not_found, server_error

from .views import HomepageView, PrivacyPolicyView, TermsOfUseView, RequestAnInviteView, ContactUsView

urlpatterns = [
    # Homepage
    url(
        r'^$',
        view=HomepageView.as_view(),
        name="home"
    ),
    url(r'^account/',
        include('account.urls')
    ),
    url(r'^portfolio/',
        include('portfolios.urls', namespace="portfolios")
    ),
    url(r'^privacy_policy$', PrivacyPolicyView.as_view(), name='privacy_policy'),
    url(r'^terms_of_use$', TermsOfUseView.as_view(), name='terms_of_use'),
    url(r'^contact_us$', ContactUsView.as_view(), name='contact_us'),

    url(r'^request_an_invite$', include('invitation.urls', namespace='invitation')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [

        url(r'^__debug__/$',
            include(debug_toolbar.urls)
        ),

        # This allows the error pages to be debugged during development, just visit
        # these URLs in browser to see how these error pages look.
        url(
            regex=r'^400/$',
            view=bad_request,
            name='400_demo'
        ),
        url(
            regex=r'^403/$',
            view=permission_denied,
            name='403_demo'
        ),
        url(
            regex=r'^404/$',
            view=page_not_found,
            name='404_demo'
        ),
        url(
            regex=r'^500/$',
            view=server_error,
            name='500_demo'
        ),

    ]


# URLs for non-local apps
urlpatterns += [
    
    # Django Admin Documentation
    url(r'^admin/doc/',
        include('django.contrib.admindocs.urls')
    ),

    # Django Admin
    url(r'^admin/',
        include(admin.site.urls)
    ),

    # Third-party app URLs
    url(r'^captcha/',
        include('captcha.urls')
    ),
    url(r'^markdown/',
        include('django_markdown.urls')
    ),

]
