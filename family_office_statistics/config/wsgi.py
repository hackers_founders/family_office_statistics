"""
WSGI config for this project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/dev/howto/deployment/wsgi/
"""

import sys
from os import environ
from os.path import abspath, dirname

PROJ_DIR = dirname(dirname(abspath(__file__)))
sys.path.append(PROJ_DIR)

environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()