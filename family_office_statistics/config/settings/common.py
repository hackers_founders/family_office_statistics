"""Common settings and globals for this project."""

from __future__ import absolute_import, unicode_literals
from os.path import abspath, basename, dirname, join, normpath

from django.contrib.messages import constants as messages
from django.core.urlresolvers import reverse_lazy

SETTINGS_DIR = normpath(dirname(abspath(__file__)))
CONFIG_DIR = normpath(dirname(SETTINGS_DIR))
PROJECT_DIR = normpath(dirname(CONFIG_DIR))
REPO_DIR = normpath(dirname(PROJECT_DIR))
ROOT_DIR = normpath(dirname(REPO_DIR))
SITE_NAME = basename(PROJECT_DIR)


DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Optional Django apps:
    'django.contrib.admindocs',
    'django.contrib.humanize',
    'django.contrib.sites',
)

THIRD_PARTY_APPS = (

    # Pinax apps
    'account',
    'bootstrapform',
    'metron',
    'pinax_theme_bootstrap',
    'pinax.eventlog',

    # Other
    'captcha',
    'debug_toolbar',
    'django_extensions',
    'django_markdown',
    'formtools',
    'localflavor',
)



LOCAL_APPS = (
    'utils',
    'portfolios',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    #for django debug toolbar\
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',

    'account.middleware.LocaleMiddleware',
    'account.middleware.TimezoneMiddleware',
)

MESSAGE_TAGS = {
    messages.DEBUG: 'well',
    messages.INFO: 'alert alert-info',
    messages.SUCCESS: 'alert alert-success',
    messages.WARNING: 'alert alert-warning',
    messages.ERROR: 'alert alert-danger'
}

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'

TEMPLATES = (
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': (
            normpath(join(PROJECT_DIR, 'templates')),
        ),
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': (
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                # Pinax
                'account.context_processors.account',
                'pinax_theme_bootstrap.context_processors.theme',

                # Local
                'portfolios.context_processors.user_portfolio',
            ),
        },
    },
)

MIGRATION_MODULES = {
    'auth': 'migration_modules.contrib.auth.migrations',
    'contenttypes': 'migration_modules.contrib.contenttypes.migrations',
    'sessions': 'migration_modules.contrib.sessions.migrations',
    'sites': 'migration_modules.contrib.sites.migrations',
}

SITE_ID = 1

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/assets/static/'
STATICFILES_DIRS = (
    normpath(join(PROJECT_DIR, 'static')),
)
MEDIA_URL = '/assets/media/'

STATIC_ROOT = normpath(join(ROOT_DIR, 'assets', 'static'))
MEDIA_ROOT = normpath(join(ROOT_DIR, 'assets', 'uploads'))


ACCOUNT_LOGIN_URL = reverse_lazy('account_login')
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_SIGNUP_REDIRECT_URL = reverse_lazy('portfolios:create')
ACCOUNT_LOGIN_REDIRECT_URL = reverse_lazy('home')
ACCOUNT_LOGOUT_REDIRECT_URL = reverse_lazy('home')
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 2
ACCOUNT_USE_AUTH_AUTHENTICATE = True

LOGIN_URL = ACCOUNT_LOGIN_URL
LOGIN_REDIRECT_URL = ACCOUNT_LOGOUT_REDIRECT_URL
LOGOUT_URL = reverse_lazy('account_logout')

AUTHENTICATION_BACKENDS = (
    "account.auth_backends.UsernameAuthenticationBackend",
)


MARKDOWN_EDITOR_SKIN = 'simple'

#djdt - username = admin login
def show_toolbar(request):
    if not request.is_ajax() and request.user and request.user.username == "":
        return True
    return False

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK' : 'config.settings.common.show_toolbar',
}

