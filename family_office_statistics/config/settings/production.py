"""Production settings and globals."""

from __future__ import absolute_import, unicode_literals
from os.path import join, dirname, normpath
import json

from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import ImproperlyConfigured

from .common import *


DEBUG = False
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

with open(normpath(join(ROOT_DIR, 'secrets.json'))) as secrets_file:
    secrets = json.loads(secrets_file.read())

def get_secret(setting, secrets=secrets):
    """Get the secrets variable or return explicit exception."""
    try:
        return secrets[setting]
    except KeyError:
        error_msg = "Set the {0} value".format(setting)
        raise ImproperlyConfigured(error_msg)


SECRET_KEY = get_secret('DJANGO_SECRET_KEY')
ALLOWED_HOSTS = get_secret('DJANGO_ALLOWED_HOSTS')

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = get_secret('DJANGO_EMAIL_HOST')
EMAIL_HOST_PASSWORD = str(get_secret('DJANGO_EMAIL_HOST_PASSWORD'))
EMAIL_HOST_USER = get_secret('DJANGO_EMAIL_HOST_USER')
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
EMAIL_PORT = get_secret('DJANGO_EMAIL_PORT')
EMAIL_SUBJECT_PREFIX = ''
EMAIL_USE_TLS = True
SERVER_EMAIL = EMAIL_HOST_USER

DATABASES = {
    'default': {
        'ENGINE': "django.db.backends.postgresql_psycopg2",     # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': get_secret('DJANGO_DB_NAME'),                   # Or path to database file if using sqlite3.
        'USER': get_secret('DJANGO_DB_USERNAME'),               # Not used with sqlite3.
        'PASSWORD': get_secret('DJANGO_DB_PASSWORD'),           # Not used with sqlite3.
        'HOST': get_secret('DJANGO_DB_HOST'),                   # Set to empty string for localhost. Not used with sqlite3.
        'PORT': get_secret('DJANGO_DB_PORT'),                   # Set to empty string for default. Not used with sqlite3.
    }
}

ACCOUNT_EMAIL_CONFIRMATION_EMAIL = get_secret('ACCOUNT_EMAIL_CONFIRMATION_EMAIL')
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = get_secret('ACCOUNT_EMAIL_CONFIRMATION_REQUIRED')
ACCOUNT_OPEN_SIGNUP = get_secret('ACCOUNT_OPEN_SIGNUP')

if ACCOUNT_EMAIL_CONFIRMATION_REQUIRED:
    ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = reverse_lazy(
        get_secret('ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL')
    )
