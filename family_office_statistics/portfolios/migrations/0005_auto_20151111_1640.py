# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import portfolios.models


class Migration(migrations.Migration):

    dependencies = [
        ('portfolios', '0004_auto_20151005_1818'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portfolio',
            name='US_PE',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='U.S. private equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='US_corp_bonds',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='U.S. corporate bonds', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='US_land',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='U.S. land', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='US_muni_bonds',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='U.S. municipal bonds', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='US_real_estate',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='U.S. real estate (excluding land)', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='US_treasury_bonds',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='U.S. Treasury bonds', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='angel_investments',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='angel investments', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='cash',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='cash', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='collectibles',
            field=portfolios.models.AssetPercentField(help_text='e.g. art', null=True, verbose_name='collectibles', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='crypto_currencies',
            field=portfolios.models.AssetPercentField(help_text='e.g. Bitcoin', null=True, verbose_name='crypto-currencies', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='dir_lending_global_bonds',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='direct lending global bonds', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='emerging_mkt_E',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='emerging market equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='emerging_mkt_bonds',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='emerging markets bonds', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='frontier_mkt_E',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='frontier market equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='global_venture_cap',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='global venture capital', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='gold',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='gold', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='high_freq_trading',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='high-frequency trading', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='lg_cap_US_E',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='large-cap U.S. equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='lg_cap_non_US_E',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='large-cap non-U.S. equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='managed_futures',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='managed futures', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='md_cap_US_E',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='mid-cap U.S. equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='md_cap_non_US_E',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='mid-cap non-U.S. equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='non_US_PE',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='non-U.S. private equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='non_US_corp_bonds',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='non-U.S. corporate bonds', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='non_US_gov_bonds',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='non-U.S. government bonds', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='non_US_land',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='non-U.S. land', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='non_US_real_estate',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='non-U.S. real estate (excluding land)', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='other',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='other', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='other_commodities',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='other commodities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='sm_cap_US_E',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='small-cap U.S. equities', max_digits=5, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='portfolio',
            name='sm_cap_non_US_E',
            field=portfolios.models.AssetPercentField(null=True, verbose_name='small-cap non-U.S. equities', max_digits=5, decimal_places=2),
        ),
    ]
