# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portfolios', '0003_auto_20150918_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portfolio',
            name='US_real_estate',
            field=models.DecimalField(null=True, verbose_name='U.S. real estate (excluding land)', max_digits=5, decimal_places=2),
        ),
    ]
