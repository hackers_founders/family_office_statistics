# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from decimal import Decimal
from math import fsum

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from .model_fields import AssetPercentField, AssetAmountField


class PortfolioManager(models.Manager):
    def get_asset_classnames(self):
        """
        Returns a list containing the names of all the different asset
        class types in the Portfolio model.
        """
        asset_classnames = list()
        for field in self.model._meta.fields:
            # exclude field names that aren't asset classes
            if field.name not in ('id', 'user'):
                asset_classnames.append(field.name)
        return asset_classnames


@python_2_unicode_compatible
class Portfolio(models.Model):
    """
    Stores a portfolio, with a one-to-one relationship with a
    single :model:`auth.User`.

    """
    # For reference purposes; not used with actual model fields
    CATEGORY_CHOICES = ('equities', 'real_estate', 'bonds', 'commodities', 'venture', 'other')

    objects = PortfolioManager()
    
    user = models.OneToOneField(to=settings.AUTH_USER_MODEL, verbose_name=_("user"), unique=True,
                                related_name='portfolio')

    # Equities
    lg_cap_US_E = AssetPercentField(verbose_name=_("large-cap U.S. equities"))
    md_cap_US_E = AssetPercentField(verbose_name=_("mid-cap U.S. equities"))
    sm_cap_US_E = AssetPercentField(verbose_name=_("small-cap U.S. equities"))
    lg_cap_non_US_E = AssetPercentField(verbose_name=_("large-cap non-U.S. equities"))
    md_cap_non_US_E = AssetPercentField(verbose_name=_("mid-cap non-U.S. equities"))
    sm_cap_non_US_E = AssetPercentField(verbose_name=_("small-cap non-U.S. equities"))
    emerging_mkt_E = AssetPercentField(verbose_name=_("emerging market equities"))
    frontier_mkt_E = AssetPercentField(verbose_name=_("frontier market equities"))
    US_PE = AssetPercentField(verbose_name=_("U.S. private equities"))
    non_US_PE = AssetPercentField(verbose_name=_("non-U.S. private equities"))

    # Real Estate
    US_real_estate = AssetPercentField(verbose_name=_("U.S. real estate (excluding land)"))
    non_US_real_estate = AssetPercentField(verbose_name=_("non-U.S. real estate "
                                                          "(excluding land)"))
    US_land = AssetPercentField(verbose_name=_("U.S. land"))
    non_US_land = AssetPercentField(verbose_name=_("non-U.S. land"))

    # Bonds
    US_corp_bonds = AssetPercentField(verbose_name=_("U.S. corporate bonds"))
    US_treasury_bonds = AssetPercentField(verbose_name=_("U.S. Treasury bonds"))
    US_muni_bonds = AssetPercentField(verbose_name=_("U.S. municipal bonds"))
    non_US_corp_bonds = AssetPercentField(verbose_name=_("non-U.S. corporate bonds"))
    non_US_gov_bonds = AssetPercentField(verbose_name=_("non-U.S. government bonds"))
    dir_lending_global_bonds = AssetPercentField(verbose_name=_("direct lending global bonds"))
    emerging_mkt_bonds = AssetPercentField(verbose_name=_("emerging markets bonds"))

    # Commodities
    gold = AssetPercentField(verbose_name=_("gold"))
    other_commodities = AssetPercentField(verbose_name=_("other commodities"))

    # Venture Investments
    angel_investments = AssetPercentField(verbose_name=_("angel investments"))
    global_venture_cap = AssetPercentField(verbose_name=_("global venture capital"))
    
    # Other
    cash = AssetPercentField(verbose_name=_("cash"))
    managed_futures = AssetPercentField(verbose_name=_("managed futures"))
    high_freq_trading = AssetPercentField(verbose_name=_("high-frequency trading"))
    collectibles = AssetPercentField(verbose_name=_("collectibles"), help_text=_("e.g. art"))
    crypto_currencies = AssetPercentField(verbose_name=_("crypto-currencies"),
                                          help_text=_("e.g. Bitcoin"))
    other = AssetPercentField(verbose_name=_("other"))

    def __str__(self):
        return "Portfolio {}".format(self.pk)

    def get_all_fields(self):
        """Returns a list of all field names on the instance."""
        fields = list()
        for field in self._meta.fields:

            field_name = field.name        
            # resolve picklists/choices, with get_xyz_display() function
            get_choice = "get_"+field_name+"_display"
            if hasattr( self, get_choice):
                value = getattr( self, get_choice)()
            else:
                try :
                    value = getattr(self, field_name)
                except get_user_model().DoesNotExist:
                    value = None

            # only display fields with values and skip some fields entirely
            if field.editable and value and field.name not in ('id', 'user', 'name', 'slug'):
                fields.append({
                   'label': field.verbose_name, 
                   'name': field.name, 
                   'value': value,
                })

        return fields

    # def clean(self):
    #     one_hundred_percent = Decimal(100)
    #     values = [field['value'] for field in self.get_all_fields()]
    #     if not sum(values) == one_hundred_percent:
    #         raise ValidationError(message=_("ERROR: All the values must add up to 100%."))


    def get_category_total(self, category):
        if category == "equities":
            values = (
                self.lg_cap_US_E, self.md_cap_US_E, self.sm_cap_US_E, self.lg_cap_non_US_E,
                self.md_cap_non_US_E, self.sm_cap_non_US_E, self.emerging_mkt_E,
                self.frontier_mkt_E, self.US_PE, self.non_US_PE
            )
        elif category == "real_estate":
            values = (
                self.US_real_estate, self.non_US_real_estate, self.US_land, self.non_US_land
            )
        elif category == "bonds":
            values = (
                self.US_corp_bonds, self.US_treasury_bonds, self.US_muni_bonds, self.non_US_corp_bonds,
                self.non_US_gov_bonds, self.dir_lending_global_bonds, self.emerging_mkt_bonds
            )
        elif category == "commodities":
            values = (self.gold, self.other_commodities)
        elif category == "venture":
            values = (self.angel_investments, self.global_venture_cap)
        elif category == "other":
            values = (
                self.cash, self.managed_futures, self.high_freq_trading, self.collectibles,
                self.crypto_currencies, self.other
            )
        else:
            values = ()

        return fsum(values)


@python_2_unicode_compatible
class AmountsPortfolio(models.Model):
    """
    Stores a portfolio, with a one-to-one relationship with a
    single :model:`auth.User`.

    """
    # For reference purposes; not used with actual model fields
    CATEGORY_CHOICES = ('equities', 'real_estate', 'bonds', 'commodities', 'venture', 'other')

    objects = PortfolioManager()
    
    user = models.OneToOneField(to=settings.AUTH_USER_MODEL, verbose_name=_("user"), unique=True,
                                related_name='amounts_portfolio')

    # Equities
    lg_cap_US_E = AssetAmountField(verbose_name=_("large-cap U.S. equities"))
    md_cap_US_E = AssetAmountField(verbose_name=_("mid-cap U.S. equities"))
    sm_cap_US_E = AssetAmountField(verbose_name=_("small-cap U.S. equities"))
    lg_cap_non_US_E = AssetAmountField(verbose_name=_("large-cap non-U.S. equities"))
    md_cap_non_US_E = AssetAmountField(verbose_name=_("mid-cap non-U.S. equities"))
    sm_cap_non_US_E = AssetAmountField(verbose_name=_("small-cap non-U.S. equities"))
    emerging_mkt_E = AssetAmountField(verbose_name=_("emerging market equities"))
    frontier_mkt_E = AssetAmountField(verbose_name=_("frontier market equities"))
    US_PE = AssetAmountField(verbose_name=_("U.S. private equities"))
    non_US_PE = AssetAmountField(verbose_name=_("non-U.S. private equities"))

    # Real Estate
    US_real_estate = AssetAmountField(verbose_name=_("U.S. real estate (excluding land)"))
    non_US_real_estate = AssetAmountField(verbose_name=_("non-U.S. real estate "
                                                          "(excluding land)"))
    US_land = AssetAmountField(verbose_name=_("U.S. land"))
    non_US_land = AssetAmountField(verbose_name=_("non-U.S. land"))

    # Bonds
    US_corp_bonds = AssetAmountField(verbose_name=_("U.S. corporate bonds"))
    US_treasury_bonds = AssetAmountField(verbose_name=_("U.S. Treasury bonds"))
    US_muni_bonds = AssetAmountField(verbose_name=_("U.S. municipal bonds"))
    non_US_corp_bonds = AssetAmountField(verbose_name=_("non-U.S. corporate bonds"))
    non_US_gov_bonds = AssetAmountField(verbose_name=_("non-U.S. government bonds"))
    dir_lending_global_bonds = AssetAmountField(verbose_name=_("direct lending global bonds"))
    emerging_mkt_bonds = AssetAmountField(verbose_name=_("emerging markets bonds"))

    # Commodities
    gold = AssetAmountField(verbose_name=_("gold"))
    other_commodities = AssetAmountField(verbose_name=_("other commodities"))

    # Venture Investments
    angel_investments = AssetAmountField(verbose_name=_("angel investments"))
    global_venture_cap = AssetAmountField(verbose_name=_("global venture capital"))
    
    # Other
    cash = AssetAmountField(verbose_name=_("cash"))
    managed_futures = AssetAmountField(verbose_name=_("managed futures"))
    high_freq_trading = AssetAmountField(verbose_name=_("high-frequency trading"))
    collectibles = AssetAmountField(verbose_name=_("collectibles"), help_text=_("e.g. art"))
    crypto_currencies = AssetAmountField(verbose_name=_("crypto-currencies"),
                                          help_text=_("e.g. Bitcoin"))
    other = AssetAmountField(verbose_name=_("other"))

    def __str__(self):
        return "Portfolio (amounts) {}".format(self.pk)
