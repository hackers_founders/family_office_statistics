from __future__ import absolute_import, unicode_literals
from decimal import Decimal

from django.db import models
from django.utils.translation import ugettext_lazy as _


class AssetPercentField(models.DecimalField):

    description = _("Subclass of ``models.DecimalField``")

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'max_digits': 5,
            'decimal_places': 2,
            'null': True
        })
        super(AssetPercentField, self).__init__(*args, **kwargs)
        


class AssetAmountField(models.DecimalField):

    description = _("Subclass of ``models.DecimalField``")

    def __init__(self, *args, **kwargs):
        kwargs.update({
            'max_digits': 15,
            'decimal_places': 2,
            'default': Decimal(0)
        })
        super(AssetAmountField, self).__init__(*args, **kwargs)
