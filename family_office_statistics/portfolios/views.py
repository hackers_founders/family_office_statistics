# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals, division
from math import floor, fsum
from decimal import Decimal

from django.conf import settings
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import ValidationError, PermissionDenied
from django.core.urlresolvers import reverse_lazy
from django.forms.models import model_to_dict
from django.shortcuts import redirect, get_object_or_404
from django.utils.translation import ugettext as _
from django.views.generic import (TemplateView, DetailView, FormView,
    CreateView, UpdateView, DeleteView)

from braces.views import LoginRequiredMixin, UserPassesTestMixin
from formtools.wizard.views import NamedUrlSessionWizardView

from . import forms
from .models import Portfolio, AmountsPortfolio
from .utils import get_all_averages, get_category_average


class PortfolioView(LoginRequiredMixin, DetailView):
    """
    Display an individual user's :model:`portfolios.Portfolio`.

    **Context**

    ``user_portfolio``
        An instance of :model:`portfolios.Portfolio` belonging to the
        current authenticated user.
    ``tabular_data``
        A list of three-item lists. In order, the three items are:
            - Label for asset class type
            - Value for the user portfolio's asset class type
            - Average value for that asset class type across all
              :model:`portfolios.Portfolio` instances in the database.
    
    """
    context_object_name = "portfolio"
    template_name = 'portfolios/portfolio_detail.html'

    def get_object(self):
        queryset = Portfolio.objects.all()
        if queryset.filter(user=self.request.user).exists():
            obj = queryset.get(user=self.request.user)
        else:
            obj = None
        return obj

    def get_context_data(self, **kwargs):
        context = super(PortfolioView, self).get_context_data(**kwargs)

        amounts_queryset = AmountsPortfolio.objects.filter(user=self.request.user)
        context['dollar_amounts_used'] = amounts_queryset.exists()

        portfolio = self.get_object()
        if portfolio is not None:
            data = [[field['label'], float(field['value'])] for field in portfolio.get_all_fields()]
            averages = get_all_averages()
            for i in range(len(data)):
                data[i].append(float(averages[i]))

            context['tabular_data'] = data
            
            for choice in Portfolio.CATEGORY_CHOICES:
                context['your_{}_total'.format(choice)] = portfolio.get_category_total(choice)
                context['{}_average'.format(choice)] = float(get_category_average(choice))

        return context


class PortfolioCreationMethodChoiceView(LoginRequiredMixin, TemplateView):
    template_name = "portfolios/creation_method_choice.html"

    def dispatch(self, request, *args, **kwargs):
        user_amounts_portfolio = AmountsPortfolio.objects.filter(user=request.user)
        user_portfolio = Portfolio.objects.filter(user=request.user)
        if user_amounts_portfolio.exists() or user_portfolio.exists():
            raise PermissionDenied()
        return super(PortfolioCreationMethodChoiceView, self).dispatch(request, *args, **kwargs)


class PortfolioPercentagesCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """
    Simple view for creating an individual user's
    :model:`portfolios.Portfolio`, with all form fields on one page.

    """
    form_class = forms.PortfolioPercentagesForm
    template_name = 'portfolios/portfolio_percentages_create_form.html'
    success_url = reverse_lazy('portfolios:detail')
    success_message = "Your portfolio was successfully created."

    def dispatch(self, request, *args, **kwargs):
        user_amounts_portfolio = AmountsPortfolio.objects.filter(user=request.user)
        user_portfolio = Portfolio.objects.filter(user=request.user)
        if user_amounts_portfolio.exists() or user_portfolio.exists():
            raise PermissionDenied()
        return super(PortfolioPercentagesCreateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(PortfolioPercentagesCreateView, self).form_valid(form)


class PortfolioAmountsCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """
    Create an individual user's :model:`portfolios.portfolio`.

    """
    model = AmountsPortfolio
    form_class = forms.PortfolioAmountsForm
    template_name = 'portfolios/portfolio_amounts_form.html'
    success_url = reverse_lazy('portfolios:detail')
    success_message = "Your portfolio was successfully created."

    def dispatch(self, request, *args, **kwargs):
        user_amounts_portfolio = AmountsPortfolio.objects.filter(user=request.user)
        user_portfolio = Portfolio.objects.filter(user=request.user)
        if user_amounts_portfolio.exists() or user_portfolio.exists():
            raise PermissionDenied()
        return super(PortfolioAmountsCreateView, self).dispatch(request, *args, **kwargs)
    
    def form_valid(self, form):
        data = form.cleaned_data
        total = Decimal(fsum(data.values()))
        percentages = dict()

        for label, val in data.items():
            percentages[label] = val/total * 100

        new_portfolio = Portfolio.objects.create(user=self.request.user, **percentages)

        form.instance.user = self.request.user

        return super(PortfolioAmountsCreateView, self).form_valid(form)


class PortfolioPercentagesUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """
    Update an individual user's existing :model:`portfolios.Portfolio`.

    """
    form_class = forms.PortfolioPercentagesForm
    template_name = 'portfolios/portfolio_percentages_update_form.html'
    success_url = reverse_lazy('portfolios:detail')
    success_message = "Your portfolio was successfully updated."

    def dispatch(self, request, *args, **kwargs):
        if AmountsPortfolio.objects.filter(user=request.user).exists():
            raise PermissionDenied()
        return super(PortfolioPercentagesUpdateView, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        user_portfolio = get_object_or_404(Portfolio, user=self.request.user)
        return user_portfolio


class PortfolioAmountsUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """
    Update an individual user's existing :model:`portfolios.Portfolio`.

    """
    form_class = forms.PortfolioAmountsForm
    template_name = 'portfolios/portfolio_amounts_form.html'
    success_url = reverse_lazy('portfolios:detail')
    success_message = "Your portfolio was successfully updated."

    def get_object(self):
        user_portfolio = get_object_or_404(AmountsPortfolio, user=self.request.user)
        return user_portfolio

    def form_valid(self, form):
        data = form.cleaned_data
        total = Decimal(fsum(data.values()))
        percentages = dict()

        for label, val in data.items():
            percentages[label] = val/total * 100

        Portfolio.objects.filter(user=self.request.user).update(**percentages)

        return super(PortfolioAmountsUpdateView, self).form_valid(form)


class PortfolioDeleteView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    """
    Delete an individual user's existing :model:`portfolio.Portfolio`.

    """
    template_name = "portfolios/portfolio_confirm_delete.html"
    success_url = reverse_lazy('home')
    success_message = "Your portfolio was successfully deleted."

    def get_object(self):
        user_portfolio = get_object_or_404(Portfolio, user=self.request.user)
        return user_portfolio

    def delete(self, request, *args, **kwargs):
        AmountsPortfolio.objects.filter(user=request.user).delete()
        return super(PortfolioDeleteView, self).delete(request, *args, **kwargs)


class PortfolioWizardView(LoginRequiredMixin, NamedUrlSessionWizardView):
    """
    Step-by-step view for creating an individual user's
    :model:`portfolios.Portfolio`.

    Unlike :view:`portfolios.PortfolioPercentagesCreateView`, the form is broken
    up across multiple pages.

    """
    template_name = "portfolios/portfolio_wizard_form.html"
    form_list = (
        ("equities", forms.PortfolioEquitiesForm),
        ("real_estate", forms.PortfolioRealEstateForm),
        ("bonds", forms.PortfolioBondsForm),
        ("commodities", forms.PortfolioCommoditiesForm),
        ("venture", forms.PortfolioVentureForm),
        ("other", forms.PortfolioOtherForm),
        ("review", forms.PortfolioEmptyConfirmationForm)
    )
    instance = None

    def dispatch(self, request, *args, **kwargs):
        user_portfolio = Portfolio.objects.filter(user=request.user)
        if user_portfolio.exists():
            raise PermissionDenied("You already have a portfolio.")

        if 'wizard_portfolio_wizard_view' not in request.session:
            request.session['has_data'] = False
        else:
            request.session['has_data'] = True

        if request.session['has_data']:
            if 'all_steps_visited' not in request.session['wizard_portfolio_wizard_view']:
                request.session['wizard_portfolio_wizard_view']['all_steps_visited'] = False

        return super(PortfolioWizardView, self).dispatch(request, *args, **kwargs)

    def get_form_instance(self, step):
        if self.instance is None:
            self.instance = Portfolio(user=self.request.user)
        return self.instance

    def get_form_initial(self, step):
        return self.initial_dict.get(step, {'user': self.request.user})

    def get_form_kwargs(self, step):
        if step == 'review':
            return {'request': self.request}
        else:
            return {}

    def get_context_data(self, form, **kwargs):
        context = super(PortfolioWizardView, self).get_context_data(form=form, **kwargs)

        context['wizard_data'] = self.request.session['wizard_portfolio_wizard_view']['extra_data']
        context['all_steps_visited'] = self.request.session['wizard_portfolio_wizard_view']['all_steps_visited']

        if self.steps.current == 'review':
            context['cleaned_data'] = self.get_all_cleaned_data()
        if self.request.session['has_data']:
            if 'total' in self.request.session['wizard_portfolio_wizard_view']:
                context['total'] = float(self.request.session['wizard_portfolio_wizard_view'] \
                                                             ['total'])
                context['allocations_remaining'] = 100 - context['total']
            else:
                context['total'] = None
                context['allocations_remaining'] = None

        return context


    def process_step(self, form):
        form_step_dict = self.get_form_step_data(form)
        keys = form_step_dict.keys()

        keys.remove('csrfmiddlewaretoken')
        keys.remove('portfolio_wizard_view-current_step')

        values = [Decimal(str(form_step_dict[key])) for key in keys]

        if not self.steps.current == 'review':
            self.request.session['wizard_portfolio_wizard_view'] \
                                ['extra_data'][self.steps.current] = str(sum(values))

        percentages = self.request.session['wizard_portfolio_wizard_view']['extra_data']
        percentages = [Decimal(str(item[1])) for item in percentages.items()]
        self.request.session['wizard_portfolio_wizard_view'] \
                            ['total'] = str(sum(percentages))

        categories = ("equities", "real_estate", "bonds", "commodities", "venture", "other")

        if all(cat in self.request.session['wizard_portfolio_wizard_view'] \
                                             ['extra_data'] for cat in categories):
            self.request.session['wizard_portfolio_wizard_view']['all_steps_visited'] = True

        return self.get_form_step_data(form)


    def done(self, form_list, **kwargs):
        self.instance.save()
        messages.success(self.request, "Your portfolio was successfully created.")
        return redirect('portfolios:detail')


# portfolio_wizard = PortfolioWizardView.as_view(url_name='portfolios:wizard_step')