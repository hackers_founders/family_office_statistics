# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from decimal import Decimal

from django import forms
from django.core.exceptions import ImproperlyConfigured
from django.core.validators import ValidationError
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from utils.form_mixins import WidgetTweaksMixin
from .models import Portfolio, AmountsPortfolio


PORTFOLIO_FORM_FIELDS = tuple([field.name for field in Portfolio._meta.fields \
                               if not field.auto_created and not field.is_relation])


class PortfolioPercentagesForm(forms.ModelForm):
    """
    All-in-one form for creating/updating an instance of
    `portfolios.Portfolio`.
    """

    widget_tweaks = {field: {'class': 'form-control'} for field in PORTFOLIO_FORM_FIELDS}


    class Meta:
        model = Portfolio   
        fields = PORTFOLIO_FORM_FIELDS

    def clean(self):
        one_hundred_percent = Decimal(100)
        if not self.errors:
            # import ipdb; ipdb.set_trace()
            if not sum(self.cleaned_data.values()) == one_hundred_percent:
                raise ValidationError("All the percentages must add up to 100%.")
        super(PortfolioPercentagesForm, self).clean()


class PortfolioAmountsForm(WidgetTweaksMixin, forms.ModelForm):

    widget_tweaks = {field: {'class': 'form-control'} for field in PORTFOLIO_FORM_FIELDS}
    widget_tweaks[PORTFOLIO_FORM_FIELDS[0]]['autofocus'] = 'autofocus'

    class Meta:
        model = AmountsPortfolio
        fields = PORTFOLIO_FORM_FIELDS



#################### Forms for use with form wizard ####################

class CommonInputPlaceholderMixin(object):
    """
    Adds a specified html placeholder string to all fields of a form.
    """
    common_placeholder_text = None

    def __init__(self, *args, **kwargs):
        super(CommonInputPlaceholderMixin, self).__init__(*args, **kwargs)
        if self.Meta.fields is not None:
            if self.common_placeholder_text is not None:
                if type(self.common_placeholder_text) == str or type(self.common_placeholder_text) == unicode:
                    for field_name in self.Meta.fields:
                        self.fields[field_name].widget.attrs['placeholder'] = self.common_placeholder_text
                else:
                    raise ImproperlyConfigured("The value for `common_placeholder_text` must be a "
                                               "string or unicode.")
            else:
                raise ImproperlyConfigured("You must specify a value for `common_placeholder_text`.")
        else:
            raise ImproperlyConfigured("`CommonInputPlaceholderMixin` requires the `Meta.fields` "
                                       "attribute to be defined.")


class PortfolioEquitiesForm(forms.ModelForm):
    class Meta:
        model = Portfolio
        fields = (
            'lg_cap_US_E',
            'md_cap_US_E',
            'sm_cap_US_E',
            'lg_cap_non_US_E',
            'md_cap_non_US_E',
            'sm_cap_non_US_E',
            'emerging_mkt_E',
            'frontier_mkt_E',
            'US_PE',
            'non_US_PE'
            )

class PortfolioRealEstateForm(forms.ModelForm):
    class Meta:
        model = Portfolio
        fields = (
            'US_real_estate',
            'non_US_real_estate',
            'US_land',
            'non_US_land'
            )
        
class PortfolioBondsForm(forms.ModelForm):
    class Meta:
        model = Portfolio
        fields = (
            'US_corp_bonds',
            'US_treasury_bonds',
            'US_muni_bonds',
            'non_US_corp_bonds',
            'non_US_gov_bonds',
            'dir_lending_global_bonds',
            'emerging_mkt_bonds'
            )

class PortfolioCommoditiesForm(forms.ModelForm):
    class Meta:
        model = Portfolio
        fields = (
            'gold',
            'other_commodities'
            )

class PortfolioVentureForm(forms.ModelForm):
    class Meta:
        model = Portfolio
        fields = (
            'angel_investments',
            'global_venture_cap'
            )

class PortfolioOtherForm(forms.ModelForm):
    class Meta:
        model = Portfolio
        fields = (
            'cash',
            'managed_futures',
            'high_freq_trading',
            'collectibles',
            'crypto_currencies',
            'other'
        )

class PortfolioEmptyConfirmationForm(forms.ModelForm):
    class Meta:
        model = Portfolio
        fields = ()

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(PortfolioEmptyConfirmationForm, self).__init__(*args, **kwargs)
    
    def clean(self):
        one_hundred_percent = Decimal(100)
        total = Decimal(self.request.session['wizard_portfolio_wizard_view']['total'])
        if not total == one_hundred_percent:
            raise ValidationError(mark_safe("<strong>All the percentages must add up to 100%.</strong> Recheck the numbers"
                                              " and try again."))
        else:
            self._validate_unique = True
            return self.cleaned_data