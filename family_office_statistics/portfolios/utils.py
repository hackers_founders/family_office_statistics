# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, unicode_literals
from decimal import Decimal

from django.db.models import Avg
from .models import Portfolio


def get_all_averages():
    """
    Returns a list of values (i.e. the average/mean percentage) for each
    asset class type.
    """
    portfolios = Portfolio.objects.all()
    category_list = Portfolio.objects.get_asset_classnames()
    dictionary_list = [portfolios.aggregate(Avg(category)) for category in category_list]
    averages = [avg for dictionary in dictionary_list for avg in dictionary.itervalues()]

    return averages


def get_category_average(category):
    """
    Returns the average/mean percentage of the specified category.


    **Keyword arguments**

        ``category``
            one of six possible strings representing a broad asset
            class category:
                'equities', 'real_estate', 'bonds', 'commodities',
                'venture', 'other'
    """
    portfolio_list = Portfolio.objects.all()
    valid_args = Portfolio.CATEGORY_CHOICES
    if category in valid_args:
        values = [portfolio.get_category_total(category) for portfolio in portfolio_list]
        average = sum(values)/len(values)
        return average
    else:
        pass